var gulp = require('gulp');
var plugins = require("gulp-load-plugins")({});
plugins.del = require("del");

var paths = {
    scripts: {
        src: './js/',
        files: [
            './js/d3.min.js',
            './js/jquery-2.1.3.min.js',
            './bower_components/angular/angular.min.js',
            './js/fancybox/jquery.fancybox-1.3.4.js',
            './js/map/raphael.js',
            './js/app.js',
            './js/map/mapCtrl.js',
            './js/map/kgMapDrt.js',
            './js/diseasesChart/diseasesChartCtrl.js',
            './js/diseasesChart/diseasesChartDrt.js',
            './js/**/*.js'],
        dest: 'dist/js/'
    },
    images: {
        src: './images/',
        files: './images/**/*',
        dest: 'dist/images/'
    },
    styles: {
        src: './css/',
        files: './css/**/*.*ss',
        dest: 'dist/css/'
    }
};

gulp.task('styles', function () {
    return gulp.src(paths.styles.files)
        //.pipe(plugins.changed(paths.styles.src))
        //.pipe(plugins.rubySass({style: 'compact', "sourcemap=none": true}))
        .pipe(plugins.concat('styles.css'))
        //.pipe(plugins.autoprefixer('last 2 version', 'safari 5', 'opera 12.1', 'ios 6', 'android 4'))
        //.pipe(console.log('autoprefixed'))
        .pipe(gulp.dest(paths.styles.dest))
        .pipe(plugins.rename({suffix: '.min'}))
        .pipe(plugins.minifyCss())
        //.pipe(console.log('minified'))
        .pipe(gulp.dest(paths.styles.dest));
});

gulp.task('scripts', function () {
    return gulp.src(paths.scripts.files)
        //.pipe(plugins.changed(paths.scripts.src))
        .pipe(plugins.concat('main.js'))
        .pipe(gulp.dest(paths.scripts.dest))
        .pipe(plugins.rename({suffix: '.min'}))
        .pipe(plugins.uglify())
        .pipe(gulp.dest(paths.scripts.dest));
});

gulp.task('images', function () {
    return gulp.src(paths.images.files)
        //.pipe(plugins.changed(paths.images.src))
        .pipe(plugins.cache(plugins.imagemin({optimizationLevel: 3, progressive: true, interlaced: true})))
        .pipe(gulp.dest(paths.images.dest));
});

gulp.task('clean', function (cb) {
    plugins.del([paths.scripts.dest, paths.images.dest, paths.styles.dest], cb)
});

gulp.task('default', ['clean'], function () {
    gulp.start('styles', 'scripts', 'images');
});

gulp.task('watch', function () {
    gulp.watch(paths.styles.files, ['styles']);
    gulp.watch(paths.scripts.files, ['scripts']);
    gulp.watch(paths.images.files, ['images']);
});
