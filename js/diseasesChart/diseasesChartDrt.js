App.directive('diseasesChart', function () {
    return {
        restrict: 'E',
        replace: true,
        templateUrl: 'templates/diseases.html',
        controller: 'DiseasesChartCtrl'
    }
});