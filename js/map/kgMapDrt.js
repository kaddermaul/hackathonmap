App.directive('map', function () {
    return {
        restrict: 'E',
        replace: true,
        templateUrl: 'templates/map.html',
        controller: 'MapCtrl'
    }
});